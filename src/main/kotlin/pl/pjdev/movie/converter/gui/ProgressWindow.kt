package pl.pjdev.movie.converter.gui

import org.apache.commons.lang3.time.DurationFormatUtils
import java.awt.*
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import javax.swing.*
import javax.swing.JPanel
import javax.swing.JSeparator
import javax.swing.BorderFactory


class ProgressWindow {

    private val frame = JFrame("Movie converter")

    private val fileProgressLabel = JLabel("Postęp")
    private val fileProgressBar = JProgressBar(0, 100)

    private val globalProgressLabel = JLabel("Ogólny postęp")
    private val globalProgressBar = JProgressBar()

    private var fileInformation: MediaFileInformation = MediaFileInformation("", 0L)

    fun create() {
        val container = JPanel()

        fileProgressBar.isStringPainted = true
        globalProgressBar.isStringPainted = true
        fileProgressBar.string = ""
        globalProgressBar.string = ""

        val layout = GroupLayout(container)
        layout.autoCreateGaps = true
        layout.autoCreateContainerGaps = true
        layout.setVerticalGroup(
            layout.createSequentialGroup()
                .addComponent(fileProgressLabel)
                .addComponent(fileProgressBar)
                .addComponent(globalProgressLabel)
                .addComponent(globalProgressBar)
        )
        layout.setHorizontalGroup(
            layout.createParallelGroup()
                .addComponent(fileProgressLabel)
                .addComponent(fileProgressBar)
                .addComponent(globalProgressLabel)
                .addComponent(globalProgressBar)
        )

        container.layout = layout

        frame.add(container)
        frame.pack()
        frame.setSize(frame.width * 4, frame.height)
        frame.setLocationRelativeTo(null)

        frame.defaultCloseOperation = JFrame.DO_NOTHING_ON_CLOSE
        frame.addWindowListener(object: WindowAdapter() {
            override fun windowClosing(e: WindowEvent?) {
                println("Closing!")
                System.exit(0)
            }
        })
    }

    fun show() {
        frame.isVisible = true
    }

    fun initializeGlobalProgress(fileCount: Int) {
        SwingUtilities.invokeLater {
            globalProgressBar.minimum = 0
            globalProgressBar.maximum = fileCount
            globalProgressBar.string= "0/$fileCount"
        }
    }

    fun startProcessingFile(fileName: String, duration: Long) {
        fileInformation = MediaFileInformation(fileName, duration)
        SwingUtilities.invokeLater {
            fileProgressLabel.text = "Postęp: $fileName"
            fileProgressBar.value = 0
            fileProgressBar.string = "00:00:00/00:00:00"
        }
    }

    fun endProcessingFile() {
        this.elapsedTime = 0L
        this.estimation = 0L
        SwingUtilities.invokeLater {
            fileProgressBar.value = 100
            globalProgressBar.value = globalProgressBar.value + 1
            globalProgressBar.string = "${globalProgressBar.value}/${globalProgressBar.maximum}"
        }
    }

    fun registerFileProgress(progress: Double) {
        SwingUtilities.invokeLater {
            fileProgressLabel.text = "Postęp: ${fileInformation.fileName}"
            fileProgressBar.value = progress.toInt()
        }
    }

    private var elapsedTime = 0L
    private var estimation = 0L

    fun updateElapsedTime(elapsedTime: Long) {
        this.elapsedTime = elapsedTime
        updateProgressBarInfo()
    }

    fun updateEstimation(estimation: Long) {
        this.estimation = estimation
        updateProgressBarInfo()
    }

    private fun updateProgressBarInfo() {
        val currentProcessedDuration = formatDuration(this.elapsedTime)
        val estimatedDuration = formatDuration(this.estimation)
        SwingUtilities.invokeLater {
            fileProgressBar.string = "$currentProcessedDuration/$estimatedDuration"
        }
    }

    data class MediaFileInformation(val fileName: String, val duration: Long)

    private fun formatDuration(timeInMilis: Long): String? {
        return DurationFormatUtils.formatDuration(timeInMilis, "HH:mm:ss")
    }
}