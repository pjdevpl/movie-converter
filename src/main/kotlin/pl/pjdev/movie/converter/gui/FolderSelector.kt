package pl.pjdev.movie.converter.gui

import java.io.File
import java.nio.file.Paths
import javax.swing.JFileChooser

class FolderSelector {

    fun selectFolder(): File? {
        val chooser = JFileChooser(Paths.get(".").toAbsolutePath().toString())
        chooser.fileSelectionMode = JFileChooser.DIRECTORIES_ONLY
        chooser.dialogTitle = "Wybierz folder"
        val returnValue = chooser.showOpenDialog(null)
        return if (returnValue == JFileChooser.APPROVE_OPTION) chooser.selectedFile else null
    }
}