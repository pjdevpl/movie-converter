package pl.pjdev.movie.converter.service

import net.bramp.ffmpeg.FFmpeg
import net.bramp.ffmpeg.FFmpegExecutor
import net.bramp.ffmpeg.FFprobe
import net.bramp.ffmpeg.builder.FFmpegBuilder
import org.apache.commons.lang3.time.DurationFormatUtils
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Paths
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class ConverterService(private val progressListener: ConverterProgressListener) {

    private val extensions = listOf("ts", "mkv", "mp4")
    private val outputExtension = "mp4"

    private val ffmpeg = FFmpeg(Paths.get("./ffmpeg/ffmpeg.exe").toAbsolutePath().toString())
    private val ffprobe = FFprobe(Paths.get("./ffmpeg/ffprobe.exe").toAbsolutePath().toString())
    private val executor = FFmpegExecutor(ffmpeg, ffprobe)
    private val threadExecutor = Executors.newFixedThreadPool(1)

    private val properties = Properties()

    private val logger = LoggerFactory.getLogger(ConverterService::class.java)

    init {
        properties.load(Paths.get("./properties/converter.properties").toFile().inputStream())
    }

    fun convertAllInFolder(folder: File) {
        val filesInFolder = folder.listFiles { f -> extensions.contains(f.extension) }
        logger.info("Found ${filesInFolder.size} matching files")
        progressListener.onFileCount(filesInFolder.size)

        val targetLocation = folder.toPath().resolve("converted").toFile()

        try {
            if (!targetLocation.exists()) {
                targetLocation.mkdir()
            }
        } catch (ex: Exception) {
            logger.error("Error when creating target location", ex)
        }
        logger.info("Target location created at ${targetLocation.absolutePath}")

        val width = properties.getProperty("resolution.width").toInt()
        val height = properties.getProperty("resolution.height").toInt()
        val quality = properties.getProperty("quality").toDouble()
        logger.info("Will convert files to $width x $height at $quality quality")

        filesInFolder.forEach { file ->
            val probeResult = ffprobe.probe(file.absolutePath)

            val builder = FFmpegBuilder()
                .setInput(probeResult)
                .addOutput("${file.parent}/converted/${file.nameWithoutExtension}.$outputExtension")
                .setVideoCodec("libx264")
                .setVideoResolution(width, height)
                .setConstantRateFactor(quality)
                .setPreset("slow")
                .done()

            val fileDuration = probeResult.getFormat().duration * TimeUnit.SECONDS.toNanos(1)
            logger.info("Will process file ${file.name} with a duration of ${DurationFormatUtils.formatDurationHMS(TimeUnit.NANOSECONDS.toMillis(fileDuration.toLong()))}")

            val job = executor.createJob(builder) { progress ->
                val percentage = (progress.out_time_ns / fileDuration) * 100
                progressListener.onFileProgress(
                    percentage,
                    TimeUnit.NANOSECONDS.toMillis(progress.out_time_ns)
                )
            }

            threadExecutor.submit {
                try {
                    progressListener.onFileStart(file.name, TimeUnit.NANOSECONDS.toMillis(fileDuration.toLong()))
                    logger.info("Starting processing the file ${file.name}")
                    job.run()
                    progressListener.onFileEnd()
                    logger.info("File ${file.name} processed successfully")
                } catch (ex: Exception) {
                    logger.error("Error when processing the file: ${file.name}, ${ex.message}", ex)
                }
            }
        }


        threadExecutor.awaitTermination(30, TimeUnit.DAYS)
    }
}