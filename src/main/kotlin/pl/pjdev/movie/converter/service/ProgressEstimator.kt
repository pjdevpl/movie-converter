package pl.pjdev.movie.converter.service

import com.google.common.collect.EvictingQueue
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.timer
import kotlin.math.roundToLong

class ProgressEstimator(private val estimatorListener: EstimatorListener) {

    private val state = EsimatorState()
    private var timer: Timer? = null

    fun reset() {
        timer?.cancel()
        timer = null
        state.reset()
    }

    fun init(expectedLenght: Long) {
        state.expectedLength = expectedLenght
        state.startTime = System.currentTimeMillis()

        timer = timer(period = TimeUnit.SECONDS.toMillis(1)) {
            val elapsedTime = System.currentTimeMillis() - state.startTime
            estimatorListener.updateElapsedTime(elapsedTime)
            val allTimeForDownloading = elapsedTime * state.expectedLength / Math.max(state.currentProgress, 1)
            estimatorListener.updateEstimation(state.calculateEstimation(elapsedTime, allTimeForDownloading))
        }
    }

    fun registerProgress(progress: Long) {
        state.currentProgress = progress
    }

    fun completeEstimation() {
        val elapsedTime = System.currentTimeMillis() - state.startTime
        estimatorListener.updateElapsedTime(elapsedTime)
        estimatorListener.updateEstimation(elapsedTime)
    }

    private class EsimatorState {

        var startTime = 0L
        var expectedLength = 0L
        var currentProgress = 0L

        @Suppress("UnstableApiUsage")
        private val estimations = EvictingQueue.create<Long>(5)
        private var estimation = 0L

        fun reset() {
            startTime = 0L
            expectedLength = 0L
            currentProgress = 0L
            estimations.clear()
            estimation = 0L
        }

        fun calculateEstimation(elapsedTime: Long, currentEstimation: Long): Long {
            return when {
                currentProgress == expectedLength -> elapsedTime
                currentProgress.toDouble() / expectedLength.toDouble() < 0.05 -> {
                    estimation = (estimation + currentEstimation * 2) / 3
                    estimation
                }
                else -> {
                    estimations.add(currentEstimation)
                    estimations.average().roundToLong()
                }
            }
        }

    }
}

interface EstimatorListener {
    fun updateEstimation(estimation: Long)
    fun updateElapsedTime(elapsed: Long)
}