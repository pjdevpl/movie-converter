package pl.pjdev.movie.converter.service

interface ConverterProgressListener {
    fun onFileCount(fileCount: Int)
    fun onFileStart(fileName: String, duration: Long)
    fun onFileProgress(fileProgress: Double, currentProcessedDuration: Long)
    fun onFileEnd()
}