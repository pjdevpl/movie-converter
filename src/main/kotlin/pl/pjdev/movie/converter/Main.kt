package pl.pjdev.movie.converter

import org.apache.commons.lang3.time.DurationFormatUtils
import org.slf4j.LoggerFactory
import pl.pjdev.movie.converter.gui.FolderSelector
import pl.pjdev.movie.converter.gui.ProgressWindow
import pl.pjdev.movie.converter.service.ConverterProgressListener
import pl.pjdev.movie.converter.service.ConverterService
import pl.pjdev.movie.converter.service.EstimatorListener
import pl.pjdev.movie.converter.service.ProgressEstimator
import java.lang.Exception
import javax.swing.JOptionPane
import javax.swing.UIManager
import kotlin.concurrent.thread

interface Main

fun main() {

    val logger = LoggerFactory.getLogger(Main::class.java)

    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName())

    val folderSelector = FolderSelector()
    val selectedFolder = folderSelector.selectFolder()
    logger.info("Selected folder: $selectedFolder")

    if (selectedFolder != null) {
        val progressWindow = ProgressWindow()
        try {
            progressWindow.create()
            logger.info("Main window created successfully")
        } catch (ex: Exception) {
            logger.error("Error when creating main window", ex)
        }


        val estimatorListener = object: EstimatorListener {
            override fun updateElapsedTime(elapsed: Long) {
                progressWindow.updateElapsedTime(elapsed)
            }

            override fun updateEstimation(estimation: Long) {
                progressWindow.updateEstimation(estimation)
            }
        }
        val progressEstimator = ProgressEstimator(estimatorListener)


        val timeStart = System.currentTimeMillis()

        val service = ConverterService(object : ConverterProgressListener {

            private var allFilesCount: Int = 0
            private var fileCount: Int = 0

            override fun onFileCount(fileCount: Int) {
                allFilesCount = fileCount
                progressWindow.initializeGlobalProgress(fileCount)
            }

            override fun onFileStart(fileName: String, duration: Long) {
                progressWindow.startProcessingFile(fileName, duration)
                progressEstimator.init(duration)
            }

            override fun onFileProgress(fileProgress: Double, currentProcessedDuration: Long) {
                progressWindow.registerFileProgress(fileProgress)
                progressEstimator.registerProgress(currentProcessedDuration)
            }

            override fun onFileEnd() {
                progressEstimator.completeEstimation()
                progressEstimator.reset()
                progressWindow.endProcessingFile()
                fileCount += 1
                if (fileCount == allFilesCount) {
                    val timeEnd = System.currentTimeMillis()
                    val elapsedTotalTime = timeEnd - timeStart
                    logger.info("Total $fileCount files processed in ${DurationFormatUtils.formatDurationHMS(elapsedTotalTime)}")
                    JOptionPane.showConfirmDialog(
                        null,
                        "Przetwarzanie zakończone. Przetworzono $fileCount plików",
                        "Info",
                        JOptionPane.DEFAULT_OPTION
                    )
                    System.exit(0)
                }
            }
        })

        thread(start = true) {
            service.convertAllInFolder(selectedFolder)
        }

        progressWindow.show()
    } else {
        System.exit(-1)
    }
}
